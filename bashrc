# ------------------------------------------------------------------
#
# $HOME/.bashrc for bash-3.0 (or later)
# By Max Resing <max.resing@protonmail.com>
#
# partially based on https://tldp.org/LDP/abs/html/sample-bashrc.html
# partially based on betterbash
# partially based on self-written snippets
#
# The bashrc sources other files, such as a seperate file for path defs
# and a seperate file for aliases & functions.
# Thus, the following files/links should exist:
#
#   /home/max/.dotfiles/bash_aliases
#   /home/max/.dotfiles/path
#
# ------------------------------------------------------------------


# Skip if not running interactively
[ -z "$PS1" ] && return


#
# Source global bashrc if existing
#
if [ -f /etc/bashrc ];
then
    source /etc/bashrc
fi


#
# Set $DISPLAY if not yet set.
#
if [ -z ${DISPLAY:=""} ];
then
    # Display on localhost
    DISPLAY=":0.0"
fi

export DISPLAY

#
# Set $TERM if not to be found
#
if [[ -f /lib/terminfo/x/xterm-256color ]]; then
  # minimal debian/ubuntu setup
  TERM="xterm-256color"
  stty erase '^H'
else
  # default, usually existing
  TERM="vt100"
fi

export TERM


# --- General settings ---------------------------------------------

# Set options
shopt -s cdspell
shopt -s cdable_vars
shopt -s checkhash
shopt -s sourcepath
shopt -s no_empty_cmd_completion
shopt -s cmdhist
shopt -s histappend histreedit histverify
shopt -s extglob

# check window size after each command & update lines if necessary
shopt -s checkwinsize

# vim keybindings
set -o vi

# Unset options
shopt -u mailwarn
unset MAILCHECK

# --- Color definitions --------------------------------------------

# Reset colors
Normal='\e[0;0m'
BNormal="$Normal\e[1m"

# Normal Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

NC="\e[m"               # Color Reset

ALERT=${BWhite}${On_Red} # Bold White on red background

# Setup color prompt
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac


# --- System information -------------------------------------------

export XDG_CONFIG_HOME=$HOME/.dotfiles/config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state


# number of processors
NCPU=$(grep -c 'processor' /proc/cpuinfo)
# small, medium, xlarge load
SLOAD=$(( 100*${NCPU} ))
MLOAD=$(( 200*${NCPU} ))
XLOAD=$(( 400*${NCPU} ))

# Systemload as percentage
function load()
{
    local SYSLOAD=$(cut -d " " -f1 /proc/loadavg | tr -d '.')
    echo $((10#$SYSLOAD))
}

function load_color()
{
    local SYSLOAD=$(load)
    if [ ${SYSLOAD} -gt ${XLOAD} ]; then
        echo -en ${ALERT}
    elif [ ${SYSLOAD} -gt ${MLOAD} ]; then
        echo -en ${Red}
    elif [ ${SYSLOAD} -gt ${SLOAD} ]; then
        echo -en ${Yellow}
    else
        echo -en ${Green}
    fi
}

# TODO: Disk usage

# TODO: Running & suspended jobs


# --- MODT & other -------------------------------------------------

echo -e " "
echo -e "  $(date) "
echo -e "  ${Blue}User:   ${BNormal}$(whoami) ${Blue}; Login: ${BNormal}$(logname)"
echo -e "  ${Blue}Active: ${BNormal}$(users) "
echo -e "  ${Blue}Uptime: ${BNormal}$(uptime -p) "
echo -e "  ${Blue}Avg Load: $(load_color)$(load) %"
echo -e "  ${Blue}Bash Version: ${BNormal}${BASH_VERSION} "
echo -e "  ${Normal}"


# --- Shell Prompt -------------------------------------------------

# Coloring User type
if [[ ${USER} == "root" ]]; then
    SU=${BRed}
elif [[ ${USER} != $(logname) ]]; then
    SU=${BYellow}
else
    SU=${Normal}
fi

# Coloring host
if [ -n "${SSH_CONNECTION}" ]; then
  # Connected via SSH
  CNX=${BWhite}${On_Purple}
elif [[ "${DISPLAY%%:0*}" ]]; then
  # Remote machine not connected via SSH
  CNX=${ALERT}
else
  CNX=${Purple}
fi


# Prompt
PROMPT="${SU}\u${Normal}@${CNX}\h${Normal} \W"
PS1="\[${PROMPT}\]${Normal}\$ ${Normal}"

# Timeformat
export TIMEFORMAT=$'\nreal %3R\nuser %3U\nsys %3S\npcpu %P\n'

# History options
export HISTIGNORE="&:bg:fg:ll:h"
export HISTTIMEFORMAT="$(echo -e ${BCyan})[%d/%m %H:%M:%S]$(echo -e ${NC}) "
export HISTCONTROL=ignoreboth
export HISTSIZE=50
export HISTFILESIZE=50


# --- Environment --------------------------------------------------

# Set editor
if [ -n "$(which nvim)" ]; then
    EDITOR=$(which nvim)
elif [ -n "$(which vim)" ]; then
    EDITOR=$(which vim)
else
    EDITOR=$(which vi)
fi

export EDITOR
export VISUAL=${EDITOR}

# Git editor
export GIT_EDITOR=${EDITOR}

if [[ -d $HOME/workspace ]] ; then
  WORKSPACE=$HOME/workspace
else
  WORKSPACE=$HOME
fi

export WORKSPACE

# Pipenv
export PIPENV_VENV_IN_PROJECT=1


# --- Hosts file ---------------------------------------------------

# List of remote hosts in ~/.hosts if file exists
if [ -f $HOME/.dotfiles/hosts ]; then
    export HOSTFILE=$HOME/.dotfiles/hosts
else
    export HOSTFILE=/etc/hosts
fi


# --- SSH agent ----------------------------------------------------
# start ssh-agent if not already running
if ! pgrep -u "${USER}" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "${XDG_RUNTIME_DIR}/ssh-agent.env"
fi

if [[ ! "${SSH_AUTH_SOCK}" ]]; then
    source "${XDG_RUNTIME_DIR}/ssh-agent.env" > /dev/null
fi



# --- Path ---------------------------------------------------------

# List of path definitions in ~/.path
if [ -f $HOME/.dotfiles/path ]; then
    source $HOME/.dotfiles/path
fi


# --- Aliases ------------------------------------------------------

# bash
alias c='clear'             # clear
alias q='exit'              # quit
alias sudo='sudo '          # Enable aliases to sudo commands

# navigation 
alias ..='cd ..'            # parent dir
alias ...='cd ../..'        # two dirs up
alias ....='cd ../../..'    # three dirs up
alias .2='cd ../..'         # two dirs up
alias .3='cd ../../..'      # three dirs up
alias .4='cd ../../../..'   # four dirs up
alias .5='cd ../../../../..' # five dirs up

# file & dirs
alias cp='cp -iv'           # cp: interactive; verbose
alias mv='mv -iv'           # mv: interactive; verbose
alias rm='rm -iv'           # rm: interactive; verbose
alias mkdir='mkdir -pv'     # mkdir: parents; verbose

# ls
alias ls='ls --color=auto -lAh' # ls: almost all; human readable

# grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# edit
alias e='${EDITOR}'
alias nvim='${EDITOR}'
alias vim='${EDITOR}'

# systemd
alias ddisable='sudo systemctl disable'
alias denable='sudo systemctl enable'
alias drestart='sudo systemctl restart'
alias dstart='sudo systemctl start'
alias dstatus='sudo systemctl status'
alias dstop='sudo systemctl stop'

# git
alias ga='git add'
alias gc='git commit -m'
alias gce='git commit -e'
alias gd='git diff'
alias gp='git push'
alias gpl='git pull'
alias gr='git rm'
alias gs='git status'

# system utilities
alias df='df -hT'           # Disk usage (file system) - human readable & disk type
alias du='du -h'            # Disk usage (file sizes) - human readable

# other
alias less='less -SXc'      # less: truncate long lines; no init, repaint by clearing
alias wget='wget -c'        # wget: continue with partially downloaded file
alias clip='xclip -selection "clipboard"' # move to clipboard


# --- Functions ----------------------------------------------------

# Extract anything
function unx()
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.xz)    tar -xf $1      ;;
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}


# Shell friendly pastebin
ix() {
    local opts
    local OPTIND
    [ -f "$HOME/.netrc" ] && opts='-n'
    while getopts ":hd:i:n:" x; do
        case $x in
            h) echo "ix [-d ID] [-i ID] [-n N] [opts]"; return;;
            d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
            i) opts="$opts -X PUT"; local id="$OPTARG";;
            n) opts="$opts -F read:1=$OPTARG";;
        esac
    done
    shift $(($OPTIND - 1))
    [ -t 0 ] && {
        local filename="$1"
        shift
        [ "$filename" ] && {
            curl $opts -F f:1=@"$filename" $* ix.io/$id
            return
        }
        echo "^C to cancel, ^D to send."
    }
    curl $opts -F f:1='<-' $* ix.io/$id
}

# Load custom  file for aliases and functions
if [ -f $HOME/.dotfiles/bash_aliases ]; then
    source $HOME/.dotfiles/bash_aliases
fi

# Swap two files

swap() {
  if [ "$#" -ne 2 ]; then
    echo "Usage: swap FILE FILE"
    echo "Swaps the content of two files."
    echo ""
    echo "Requires exactly two arguments, the left and right file."
    echo ""
    echo "Part of the bashrc of Max Resing <max.resing@protonmail.com>."
    echo "Sourcecode available on <https://codeberg.org/resingm/bashrc>"

    return 1
  fi

  if [[ -e "$1" && -e "$2" ]]; then
    local TMPFILE=tmp.$$
    mv "${1}" "${TMPFILE}"
    mv "${2}" "${1}"
    mv "${TMPFILE}" "${2}"
  else
    echo "One of the arguments is not a file."
    return 1
  fi
}

if command -v pyenv &> /dev/null; then 
  export PATH="/usr/share/pyenv/plugins/pyenv-virtualenv/shims:${PATH}";

  export PYENV_VIRTUALENV_INIT=1;

  _pyenv_virtualenv_hook() {
    local ret=$?
    if [ -n "${VIRTUAL_ENV-}" ]; then
      eval "$(pyenv sh-activate --quiet || pyenv sh-deactivate --quiet || true)" || true
    else
      eval "$(pyenv sh-activate --quiet || true)" || true
    fi
    return $ret
  };
  if ! [[ "${PROMPT_COMMAND-}" =~ _pyenv_virtualenv_hook ]]; then
    PROMPT_COMMAND="_pyenv_virtualenv_hook;${PROMPT_COMMAND-}"
  fi
fi

# ################################################################
#
# Path defintions for my environment
#
# ################################################################


# PATH
# local binaries & scripts
if [[ -d $HOME/.local/bin ]] ; then
  export PATH=$HOME/.local/bin:$PATH
fi

if [[ -d $HOME/bin ]] ; then
  export PATH=$HOME/bin:$PATH
fi

# custom bash scripts
if [[ -d $WORKSPACE/bash/scripts/src ]] ; then
  # Workspace on local machine
  export PATH=$WORKSPACE/bash/scripts/src:$PATH
fi

if [[ -d /mnt/data/all/scripts/src ]] ; then
  # Shared folder on servers
  export PATH=/mnt/data/all/scripts/src:$PATH
fi

# cargo packages
if [[ -d $HOME/.cargo/bin ]] ; then
  export PATH=$HOME/.cargo/bin:$PATH
fi

# go packages
if [[ -d $HOME/.go/bin ]] ; then
  export GOPATH=$HOME/.go/:$GOPATH
  export PATH=$HOME/.go/bin:$PATH
fi

if [[ -d $HOME/go/bin ]] ; then
  export GOPATH=$HOME/go/:$GOPATH
  export PATH=$HOME/go/bin:$PATH
fi

if [[ -d $HOME/.luarocks ]] ; then
  export LUA_PATH="$HOME/.luarocks/share/lua/5.4/?.lua;$LUA_PATH"
  export LUA_PATH="$HOME/.luarocks/share/lua/5.4/?/?.lua;$LUA_PATH"
  export LUA_PATH="$HOME/.luarocks/share/lua/5.4/?/init.lua;$LUA_PATH"

  export LUA_CPATH="$HOME/.luarocks/lib/lua/5.4/?;$LUA_CPATH"
  export LUA_CPATH="$HOME/.luarocks/lib/lua/5.4/?.so;$LUA_CPATH"
fi

# poetry packages
if [[ -d $HOME/.poetry/bin ]] ; then
  export PATH=$HOME/.poetry/bin:$PATH
fi


# Python
if [[ -d $HOME/.poetry/bin ]]; then
  export PYTHONPATH=$HOME/.poetry/bin:$PYTHONPATH
fi

if [[ -f /usr/bin/pyenv ]]; then
  export PYENV_ROOT="$HOME/.pyenv"
  export PATH=$PYENV_ROOT/shims:$PATH
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
fi


# Google Cloud Platform
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/max/.local/lib/google-cloud-sdk/path.bash.inc' ]; then
  source '/home/max/.local/lib/google-cloud-sdk/path.bash.inc';
fi

# Chome export path
if [ -f '/usr/bin/chromium' ]; then
  export CHROME_EXECUTABLE=/usr/bin/chromium

fi

# Android SDK
if [ -d '/opt/android-sdk' ]; then
  export ANDROID_SDK_ROOT='/opt/android-sdk'
  export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools/
  export PATH=$PATH:$ANDROID_SDK_ROOT/tools/bin/
  export PATH=$PATH:$ANDROID_SDK_ROOT/tools/
  export PATH=$PATH:$ANDROID_SDK_ROOT/emulator/
fi


# ESP IDE
if [ -d "/home/max/.esp/esp-idf" ] ; then
  export IDF_PATH="/home/max/.esp/esp-idf"
  export IDF_PYTHON_ENV_PATH="/home/max/.pyenv/versions/3.10.9/envs/idf_python_env/bin/python"
fi

# ------------------------------------------------------------------
#
# Custom alias and function definitions for my environment
#
# ------------------------------------------------------------------


# --- Aliases ------------------------------------------------------

# --- Functions ----------------------------------------------------

#
# Open a PDF in zathura and switch to background
#

pdf() {
    # start zathura in background
    zathura $1 &
}

#
# Create filewall rules for docker container.
# Rules be cleared on reboot
#
# wlan2docker() {
#     sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
#     sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
#     sudo iptables -A FORWARD -i docker0 -o wlan0 -j ACCEPT
# }

# eth2docker() {
#     sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
#     sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
#     sudo iptables -A FORWARD -i docker0 -o eth0 -j ACCEPT
# }

# qemu-start() {
#     sudo systemctl start libvirtd.service
#     sudo systemctl start virtlogd.service
#     virt-manager
# }
#

